package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	socketio "github.com/googollee/go-socket.io"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type User struct {
	gorm.Model
	Name    string
	Config  string
	Scrapes []Scrape
}

type URL struct {
	gorm.Model
	Href string
}

type Scrape struct {
	gorm.Model
	URL   string
	Links string
}

var db *gorm.DB

var err error

func main() {

	db, err = gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=postgres sslmode=disable password=postgres")

	if err != nil {

		panic("failed to connect database")

	}

	db.LogMode(true)
	defer db.Close()

	// db.AutoMigrate(&User{})

	db.AutoMigrate(&Scrape{})
	// db.AutoMigrate(&URL{})

	r := gin.Default()
	r.Use(cors.Default())

	server := socketio.NewServer(nil)

	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		log.Println("connected:", s.ID())
		return nil
	})
	go func() {
		if err := server.Serve(); err != nil {
			log.Fatalf("socketio listen error: %s\n", err)
		}
	}()
	defer server.Close()

	r.GET("/last/:max", GetLogs)
	r.GET("/socket.io/*any", gin.WrapH(server))
	r.POST("/socket.io/*any", gin.WrapH(server))
	v1 := r.Group("/v1")
	{
		v1.GET("/list/", GetLogs)
		v1.GET("/last/:max", GetLogs)
		v1.POST("/crawl/", CrawlAndLog)
	}

	r.Run(":8081")
}

func GetLogs(c *gin.Context) {

	maxLength, err := strconv.Atoi(c.Param("max"))
	c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")

	if err != nil {
		fmt.Print(err)
		// c.String(http.StatusBadRequest, "Can't parse request!")
		// return
	}

	fmt.Println(maxLength)
	fmt.Println("called?")
	var scrapes []Scrape

	// The result is big so let's limit it to max 20
	if maxLength > 0 && maxLength <= 35 {
		db.Limit(maxLength).Order("id desc").Find(&scrapes)
	} else if maxLength > 35 {
		c.String(http.StatusInternalServerError, "Max result size can be under 20 characters!")
	} else {
		db.Limit(10).Order("id desc").Find(&scrapes)
	}

	// for index, _ := range scrapes {
	// 	fmt.Println(index)
	// }
	// c.String(http.StatusOK, "We got the logs! Or did we?")

	c.JSON(http.StatusOK, scrapes)
	// c.String(http.StatusOK, "We got the logs! Or did we? %s", scrapes[0].URL)
}

func WriteCrawl(toCrawl string) {
	site := Scrape{
		URL: toCrawl,
	}
	result := db.Create(&site)

	if result.Error != nil {
		fmt.Println(result.Error)
		// c.String(http.StatusInternalServerError, "Failed to create user: %s", result.Error)
	}

}

func CrawlSite(url string, elements string, find string, attr string) []string {

	var resultUrls []string
	// Request the HTML page.

	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// res := BasicScrape(url)

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Find the review items
	doc.Find(elements).Each(func(i int, s *goquery.Selection) {
		// For each item found, get the title
		title, _ := s.Find(find).Attr(attr)
		if title != "" {
			resultUrls = append(resultUrls, title)
			// ExampleScrape(title, ".elementor-post__title")
		}
	})

	// TODO: Check for other pages

	// Remove duplicate values and return

	return removeDuplicateStr(resultUrls)
}

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true

			// We need to save only valid URLs
			if strings.Contains(item, "http") {
				list = append(list, item)
			}
		}
	}
	return list
}

type SiteParam struct {
	URL string `form:"url"`
}

func SaveCrawlSite(toCrawl string) []string {
	urls := CrawlSite(toCrawl, "*", "a", "href")

	site := Scrape{
		URL:   toCrawl,
		Links: strings.Join(urls, ","),
	}

	db.Save(&site)
	// fmt.Println(urls[0])
	time.Sleep(time.Second)
	return urls
}

func CrawlAndLog(c *gin.Context) {
	var site SiteParam
	var urls []string
	if c.ShouldBindQuery(&site) == nil {
		log.Println("====== Only Bind By Query String ======")
		log.Println(site.URL)

		toCrawl := site.URL

		urls = SaveCrawlSite(toCrawl)
	}
	fmt.Println("CRAWL SUCCESS!!!! -----")

	for _, url := range urls {
		fmt.Println("CRAWLING ---------------------- ", url)
		SaveCrawlSite(url)
	}

	c.String(200, "Success")

	c.String(http.StatusOK, "Crawl of %s successfully logged to database")
}
